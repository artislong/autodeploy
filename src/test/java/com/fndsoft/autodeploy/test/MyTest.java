package com.fndsoft.autodeploy.test;

import com.fndsoft.ssh.utils.SSHUtils;
import com.google.common.io.Resources;
import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.lang3.StringUtils;
import org.ho.yaml.Yaml;
import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.Profile;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by 陈敏 on 2017/7/11.
 */
public class MyTest {
    @Test
    public void test01() {
        String userHome = System.getProperty("user.home");
        File file = new File(userHome + "/\\.autodeploy");
        if (!file.exists()) {
            file.mkdir();
        }
    }

    @Test
    public void test02() throws Exception {
        Config config = new Config();
        URL url = Resources.getResource("test.ini");
        config.setMultiSection(true);
        Ini ini = new Ini();
        ini.setConfig(config);
        ini.load(url);
        System.out.println(StringUtils.center("system", 50, "="));
        Profile.Section test = ini.get("test");
        for (Map.Entry<String, String> entry : test.entrySet()) {
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }
        test.put("name", "admin1");
        for (Map.Entry<String, String> entry : test.entrySet()) {
            System.out.println(entry.getKey() + "=" + entry.getValue());
        }
        ini.put("test", test);
    }

    @Test
    public void test03() {
        System.out.println(SSHUtils.ping("192.168.10.129"));
    }

    @Test
    public void test04() throws Exception {
        FileReader fr = new FileReader("E:\\Git\\respository\\autodeploy\\src\\test\\resources\\test.yml");
        Map root = (HashMap)Yaml.load(fr);
        Properties properties = new Properties();
        properties.putAll(transYmltoMap(root));
        for (Object key : properties.keySet()) {
            System.out.println(key + "=" + properties.getProperty(key.toString()));
        }
    }

    private Map transYmltoMap(Map root) {
        Map map = new HashMap();
        for (Object key : root.keySet()) {
            Object value = root.get(key);
            if (!(value instanceof Map)) {
                map.put(key.toString(), value);
            } else {
                Map child = (Map) value;
                Map newChild = new HashMap();
                for (Object childKey : child.keySet()) {
                    newChild.put(key + "." + childKey.toString(), child.get(childKey));
                }
                map.putAll(transYmltoMap(newChild));
            }
        }
        return map;
    }

    @Test
    public void test05() throws Exception {
        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("file:D:\\log_network.txt").to("sftp://192.168.10.121/home/fisCM/fisService?username=fisCM&password=fisCM123");
            }
        });
        context.start();
//        context.stop();
    }


}
