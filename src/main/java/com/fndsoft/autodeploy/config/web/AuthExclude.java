package com.fndsoft.autodeploy.config.web;

import java.lang.annotation.*;

/**
 * Created by 陈敏 on 2017/8/29.
 */
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthExclude {
}
