package com.fndsoft.autodeploy.config.web;

import com.fndsoft.autodeploy.commons.assist.args.ErrorBean;
import com.fndsoft.autodeploy.commons.assist.args.IncomeParam;
import com.fndsoft.autodeploy.commons.assist.args.ResultUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Created by 陈敏 on 2017/8/29.
 */
//@Component
//@Aspect
public class AuthInterceptor {

    public static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);

    @Autowired
    private HttpServletRequest request;

    @Around("execution(* com.fndsoft.autodeploy.controller.*.*(..))")
    public Object intercept(ProceedingJoinPoint joinPoint) throws Throwable {
        Object target = joinPoint.getTarget();
        String methodName = joinPoint.getSignature().getName();
        Class<?> targetClass = target.getClass();
        AuthExclude targetExclude = targetClass.getAnnotation(AuthExclude.class);
        if (targetExclude != null) {
            return execute(joinPoint);
        }
        Method method = targetClass.getMethod(methodName, IncomeParam.class);
        AuthExclude methodExclude = method.getAnnotation(AuthExclude.class);
        if (methodExclude != null) {
            return execute(joinPoint);
        }

        Object obj = request.getSession().getAttribute("user");
        if (ObjectUtils.isEmpty(obj)) {
            return ResultUtil.error(new ErrorBean("noLogin", "用户未登录"));
        }
        return execute(joinPoint);
    }

    private Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info(joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName() + "开始执行");
        long startTime = System.currentTimeMillis();
        Object obj = joinPoint.proceed();
        long endTime = System.currentTimeMillis();
        logger.info(joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName() + "执行结束,共耗时" + (endTime - startTime) + "毫秒");
        return obj;
    }
}
