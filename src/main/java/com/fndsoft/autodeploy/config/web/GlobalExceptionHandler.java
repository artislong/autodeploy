package com.fndsoft.autodeploy.config.web;

import com.fndsoft.autodeploy.commons.BusinessException;
import com.fndsoft.autodeploy.commons.assist.args.ErrorBean;
import com.fndsoft.autodeploy.commons.assist.args.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 陈敏 on 2017/8/29.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler
    @ResponseBody
    public Object defaultErrorHandler(HttpServletRequest request, Exception e){
        String errorMessage = "";
        String errorCode = "";
        if (e instanceof BusinessException) {
            BusinessException businessException = (BusinessException) e;
            errorMessage = businessException.getMessage();
        } else {
            errorMessage = "系统异常，请联系管理员！";
        }
        StackTraceElement[] stackTraces = e.getStackTrace();
        for (StackTraceElement stackTrace : stackTraces) {
            String className = stackTrace.getClassName();
            String methodName = stackTrace.getMethodName();
            if (className.startsWith("com.fndsoft.autodeploy")) {
                errorCode = methodName + "Error";
            }
        }
        logger.error(errorMessage, e);
        return ResultUtil.error(new ErrorBean(errorCode, errorMessage));
    }
}
