package com.fndsoft.autodeploy.config.ehcache;

import com.fndsoft.autodeploy.utils.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 陈敏 on 2017/8/28.
 */
@Component
public class CacheTemplate {

    private static final Logger logger = LoggerFactory.getLogger(CacheTemplate.class);

    @Autowired
    private EhCacheUtil ehCacheUtil;

    public <T> T get(String key, Class<T> clazz) {
        try {
            Object o = ehCacheUtil.get(key);
            return BeanUtil.transBean2Bean(o, clazz);
        } catch (Exception e) {
            logger.error("trans to bean error", e);
        }
        return null;
    }

    public Object get(String key) {
        return ehCacheUtil.get(key);
    }

    public void save(String key, Object value) {
        ehCacheUtil.save(key, value);
    }

    public void save(Object value) {
        try {
            Object key = getCacheKey(value);
            if (key == null) {
                key = value.toString();
            }
            ehCacheUtil.save(key.toString(), value);
        } catch (Exception e) {
        }

    }

    public void delete(String key) {
        ehCacheUtil.delete(key);
    }

    public void delete(Object value) {
        try {
            Object key = getCacheKey(value);
            if (key == null) {
                key = value.toString();
            }
            ehCacheUtil.delete(key.toString());
        } catch (Exception e) {
        }
    }

    public void delete(String key, Object value) {
        ehCacheUtil.delete(key);
    }

    public void clear() {
        ehCacheUtil.clear();
    }

    private Object getCacheKey(Object value) throws Exception {
        Class<?> clazz = value.getClass();
        Field[] fields = clazz.getFields();
        List list = new ArrayList();
        for (int i = 0; i < fields.length; i++) {
            CacheKey cacheKey = fields[i].getAnnotation(CacheKey.class);
            if (cacheKey != null) {
                list.add(fields[i].get(value));
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).toString());
            if (i != list.size() - 1) {
                sb.append("^");
            }
        }
        return sb;
    }

}
