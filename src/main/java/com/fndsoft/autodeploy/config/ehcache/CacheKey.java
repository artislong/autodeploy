package com.fndsoft.autodeploy.config.ehcache;

import java.lang.annotation.*;

/**
 * Created by 陈敏 on 2017/8/28.
 */
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheKey {
    String value() default "";
}
