package com.fndsoft.autodeploy.config.ehcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * Created by 陈敏 on 2017/8/28.
 */
@CacheConfig(cacheNames = "prod")
@Component
public class EhCacheUtil {
    private static final Logger logger = LoggerFactory.getLogger(EhCacheUtil.class);

    @CacheEvict(key = "#key")
    public void save(String key, Object value) {
    }

    @Cacheable(key = "#key")
    public Object get(String key) {
        logger.debug("{} cache not exist", key);
        return null;
    }

    @CachePut(key = "#key")
    public void update(String key, Object value) {
    }

    @CacheEvict(key = "#key")
    public void delete(String key) {
        logger.debug("{} cache not exist", key);
    }

    @CacheEvict(allEntries = true)
    public void clear() {
        logger.info("The ehcache will clear");
    }
}
