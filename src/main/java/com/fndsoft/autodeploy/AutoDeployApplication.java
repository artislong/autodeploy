package com.fndsoft.autodeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 陈敏 on 2017/7/11.
 */
@SpringBootApplication
public class AutoDeployApplication {
    public static void main(String[] args){
        SpringApplication.run(AutoDeployApplication.class, args);
    }
}
