package com.fndsoft.autodeploy.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.SecureRandom;

/**
 * Created by 陈敏 on 2017/7/24.
 * DES加密解密
 */
public class DES {

    /**
     * 数据加密，算法（DES）
     *
     * @param data 要进行加密的数据
     * @param password 密钥
     * @return 加密后的数据
     */
    public static String encryptBasedDes(String data, String password)
    {
        String encryptedData = null;
        try
        {
            Cipher cipher = build(password);
            // 加密，并把字节数组编码成字符串
            encryptedData = new sun.misc.BASE64Encoder().encode(cipher.doFinal(data.getBytes()));
        } catch (Exception e) {
            throw new RuntimeException("加密错误，错误信息：", e);
        }
        return encryptedData;
    }


    /**
     * 数据解密，算法（DES）
     *
     * @param data 要进行解密的数据
     * @param password 密钥
     * @return 加密后的数据
     */
    public static String decryptBasedDes(String data, String password)
    {
        String decryptedData = null;
        try
        {
            Cipher cipher = build(password);
            decryptedData = new String(cipher.doFinal(new sun.misc.BASE64Decoder().decodeBuffer(data)));
        } catch (Exception e) {
            throw new RuntimeException("解密错误，错误信息：", e);
        }
        return decryptedData;
    }

    private static Cipher build(String password) throws Exception {
        // DES算法要求有一个可信任的随机数源
        SecureRandom sr = new SecureRandom();
        DESKeySpec deskey = new DESKeySpec(password.getBytes());
        // 创建一个密匙工厂，然后用它把DESKeySpec转换成一个SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey key = keyFactory.generateSecret(deskey);
        // 加密对象
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.ENCRYPT_MODE, key, sr);
        return cipher;
    }
}
