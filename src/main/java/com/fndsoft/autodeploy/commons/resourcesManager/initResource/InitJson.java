package com.fndsoft.autodeploy.commons.resourcesManager.initResource;

import com.alibaba.fastjson.JSONObject;
import com.fndsoft.autodeploy.utils.JSONUtil;

import java.net.URL;

/**
 * Created by 陈敏 on 2017/8/29.
 */
public class InitJson {

    public static JSONObject getJSONObject(String json) {
        String os = System.getProperty("os.name");
        URL url;
        if (os.toLowerCase().startsWith("win")) {
            url = ClassLoader.getSystemResource(json);
        } else {
            url = Thread.currentThread().getContextClassLoader().getResource("/" + json);
        }
        return JSONUtil.jsonObject(url.getPath());
    }
}
