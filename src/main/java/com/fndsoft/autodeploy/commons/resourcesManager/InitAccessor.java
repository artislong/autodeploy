package com.fndsoft.autodeploy.commons.resourcesManager;

import com.fndsoft.autodeploy.commons.BusinessException;

/**
 * Created by 陈敏 on 2017/8/24.
 */
public interface InitAccessor {
    void init() throws BusinessException;
}
