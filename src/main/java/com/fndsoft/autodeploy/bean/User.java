package com.fndsoft.autodeploy.bean;

import com.fndsoft.autodeploy.commons.assist.enums.UserStatus;
import com.fndsoft.autodeploy.config.ehcache.CacheKey;
import lombok.NonNull;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by 陈敏 on 2017/8/25.
 */
@Accessors(chain = true)
@Entity
@Table(name = "t_user")
public class User implements Serializable {

    private static final long serialVersionUID = 8241173705978302713L;

    @CacheKey
    @Id
    @GeneratedValue
    @NonNull private String username;
    @NonNull private String password;
    // 用户是否可用
    private UserStatus status;

    // 可操作的项目列表
    @Transient
    private Set<String> projects = new HashSet<>();

    @java.beans.ConstructorProperties({"username", "password"})
    private User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @java.beans.ConstructorProperties({"username", "password", "status", "projects"})
    public User(String username, String password, UserStatus status, Set<String> projects) {
        this.username = username;
        this.password = password;
        this.status = status;
        this.projects = projects;
    }

    public User() {
    }

    @NonNull
    public String getUsername() {
        return this.username;
    }

    @NonNull
    public String getPassword() {
        return this.password;
    }

    public UserStatus getStatus() {
        return this.status;
    }

    public Set<String> getProjects() {
        return this.projects;
    }

    public User setUsername(@NonNull String username) {
        this.username = username;
        return this;
    }

    public User setPassword(@NonNull String password) {
        this.password = password;
        return this;
    }

    public User setStatus(UserStatus status) {
        this.status = status;
        return this;
    }

    public User setProjects(Collection<String> projects) {
        this.projects.addAll(projects);
        return this;
    }

    public String toString() {
        return "User(username=" + this.getUsername() + ", password=" + this.getPassword() + ", status=" + this.getStatus() + ", projects=" + this.getProjects() + ")";
    }

}
