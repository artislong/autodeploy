package com.fndsoft.autodeploy.bean;

import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * Created by 陈敏 on 2017/7/11.
 */
@Accessors(chain = true)
@Entity
@Table(name = "t_project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name; // 项目名称
    private String path;  // 项目在服务器上的全路径
    private String buildWay;  // 项目构建方式 javaweb/maven/gradle......
    private String deployPath;  // 项目发布目录
    private String repositoryPath;  // 项目仓库路径 git/svn
    private String startWay;  // 项目启动方式
    private String defineShell; // 自定义shell脚本，若使用自定义的shell，上面的都可以省略的
    private String ip;

    public Project() {
    }

    public Project(Long id, String name, String path, String buildWay, String deployPath, String repositoryPath, String startWay, String defineShell, String ip) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.buildWay = buildWay;
        this.deployPath = deployPath;
        this.repositoryPath = repositoryPath;
        this.startWay = startWay;
        this.defineShell = defineShell;
        this.ip = ip;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPath() {
        return this.path;
    }

    public String getBuildWay() {
        return this.buildWay;
    }

    public String getDeployPath() {
        return this.deployPath;
    }

    public String getRepositoryPath() {
        return this.repositoryPath;
    }

    public String getStartWay() {
        return this.startWay;
    }

    public String getDefineShell() {
        return this.defineShell;
    }

    public Project setId(Long id) {
        this.id = id;
        return this;
    }

    public Project setName(String name) {
        this.name = name;
        return this;
    }

    public Project setPath(String path) {
        this.path = path;
        return this;
    }

    public Project setBuildWay(String buildWay) {
        this.buildWay = buildWay;
        return this;
    }

    public Project setDeployPath(String deployPath) {
        this.deployPath = deployPath;
        return this;
    }

    public Project setRepositoryPath(String repositoryPath) {
        this.repositoryPath = repositoryPath;
        return this;
    }

    public Project setStartWay(String startWay) {
        this.startWay = startWay;
        return this;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Project setDefineShell(String defineShell) {
        this.defineShell = defineShell;
        return this;
    }

    public String toString() {
        return "Project(id=" + this.getId() + ", name=" + this.getName() + ", path=" + this.getPath() + ", buildWay=" + this.getBuildWay() + ", deployPath=" + this.getDeployPath() + ", repositoryPath=" + this.getRepositoryPath() + ", startWay=" + this.getStartWay() + ", defineShell=" + this.getDefineShell() + ", ip=" + this.getIp() + ")";
    }
}
