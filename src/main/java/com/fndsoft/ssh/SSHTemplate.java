package com.fndsoft.ssh;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.*;
import java.util.Vector;

/**
 * Created by 陈敏 on 2017/8/22.
 */
public class SSHTemplate implements InitializingBean {
    private static final Logger logger = LoggerFactory.getLogger(SSHTemplate.class);
    private SSHSource           sshSource;
    private int                 returnCode;
    public SSHTemplate() {
    }

    public SSHTemplate(SSHSource sshSource) {
        this.sshSource = sshSource;
        afterPropertiesSet();
    }

    public void close(Session session) {
        if (session == null) {
            return;
        }
        try {
            sshSource.close(session);
        } catch (Exception e) {
            logger.info("close Session error," + e.getMessage());
        }
    }

    public int close(Channel channel) {
        int returnCode = 0;
        if (channel.isClosed()) {
            returnCode = channel.getExitStatus();
        }
        if (channel != null) {
            channel.disconnect();
        }
        return returnCode;
    }

    public SSHInfo execute(String command) {
        return execute(command, null);
    }

    public SSHInfo execute(String command, InputStream inputStream) {
        afterPropertiesSet();
        Vector<String> stdout = new Vector<>();
        ChannelExec channelExec = null;
        Session session = null;
        try {
            session = getSession();
            channelExec = (ChannelExec) buildChannel(session, ChannelType.CHANNEL_EXEC_TYPE);
            channelExec.setCommand(command);
            channelExec.setInputStream(inputStream);
            logger.info("The remote command is: " + command);
            stdout.addAll(getReturnMsg(channelExec));
        } catch (Exception e) {
            logger.error("The " + command + " executed error, " + e.getMessage());
        } finally {
            returnCode = close(channelExec);
            close(session);
        }
        return new SSHInfo(returnCode, stdout);
    }

    public SSHInfo execute(InputStream inputStream) {
        afterPropertiesSet();
        Vector<String> stdout = new Vector<>();
        ChannelShell channelShell = null;
        Session session = null;
        try {
            session = getSession();
            channelShell = (ChannelShell) buildChannel(session, ChannelType.CHANNEL_EXEC_TYPE);
            channelShell.setInputStream(inputStream);
            channelShell.start();
            stdout.addAll(getReturnMsg(channelShell));
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            returnCode = close(channelShell);
            close(session);
        }
        return new SSHInfo(returnCode, stdout);
    }

    /**
     * 下载文件
     * @param downloadFile 下载文件
     */
    public byte[] downLoad(String downloadFile){
        ChannelSftp sftp = null;
        try{
            sftp = (ChannelSftp) getSession().openChannel(ChannelType.CHANNEL_SFTP_TYPE.getType());
            sftp.connect();
            InputStream inputStream = sftp.get(downloadFile);
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
            return data;
        }catch(Exception e){
            logger.error(downloadFile + "文件下载失败：", e);
        } finally {
            sftp.disconnect();
        }
        return null;
    }

    /**
     * 下载文件
     * @param downloadFile 下载文件
     */
    public void downLoad(String downloadFile, String dst){
        ChannelSftp sftp = null;
        try{
            sftp = (ChannelSftp) getSession().openChannel(ChannelType.CHANNEL_SFTP_TYPE.getType());
            sftp.connect();
            File dstFile = new File(dst);
            File file = new File(dstFile.getParent());
            if(!file.exists()) {
                file.mkdirs();
                if (dstFile.exists()) {
                    dstFile.delete();
                    dstFile.createNewFile();
                } else {
                    dstFile.createNewFile();
                }
            }
            sftp.get(downloadFile, dst);
        }catch(Exception e){
            logger.error(downloadFile + "文件下载失败：", e);
        } finally {
            sftp.disconnect();
        }
    }

    /**
     * 下载文件
     * @param directory 下载目录
     * @param downloadFile 下载文件
     */
    public void downLoad(String directory, String downloadFile, String dst){
        ChannelSftp sftp = null;
        try{
            sftp = (ChannelSftp) getSession().openChannel(ChannelType.CHANNEL_SFTP_TYPE.getType());
            sftp.connect();
            sftp.cd(directory);
            File dstFile = new File(dst);
            File file = new File(dstFile.getParent());
            if(!file.exists()) {
                file.mkdirs();
                if (dstFile.exists()) {
                    dstFile.delete();
                    dstFile.createNewFile();
                } else {
                    dstFile.createNewFile();
                }
            }
            sftp.get(downloadFile, dst);
        }catch(Exception e){
            logger.error(downloadFile + "文件下载失败：", e);
        } finally {
            sftp.disconnect();
        }
    }

    public Channel buildChannel(Session session, ChannelType channelType) throws Exception {
        return session.openChannel(channelType.getType());
    }

    public Vector<String> getReturnMsg(Channel channel) {
        BufferedReader br = null;
        Vector<String> stdout = new Vector<String>();
        try {
            channel.connect();
            br = new BufferedReader(new InputStreamReader(channel.getInputStream()));
            String line;
            while ((line = br.readLine()) != null) {
                stdout.add(line);
            }
        } catch (Exception e) {

        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stdout;
    }

    public Session getSession() {
        afterPropertiesSet();
        return sshSource.getSession();
    }

    public SSHSource getSshSource() {
        return sshSource;
    }

    @Override
    public void afterPropertiesSet() {
        if (getSshSource() == null) {
            throw new IllegalArgumentException("Property 'sshSource' is required");
        }
    }

    public int getReturnCode() {
        return this.returnCode;
    }

    public void setSshSource(SSHSource sshSource) {
        this.sshSource = sshSource;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }
}
